#!/usr/bin/env python

answers = False
with open('ejercicios.md') as input_file:
    with open('respuestas.md', 'w') as output_file:
        for line in input_file:
            if line.startswith("## Ejercicio 3"):
                answers = True
            elif line.startswith("## ¿Qué se valora"):
                answers = False
            if answers:
                if line.startswith('*') or line.startswith("## Ejercicio"):
                    output_file.write(line)

