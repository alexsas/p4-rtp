## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: La componen 1268 paquetes.
* ¿Cuánto tiempo dura la captura?: La captura dura 12.81 segundos.
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: Aproximadamente unas 100 por segundo.
* ¿Qué protocolos de nivel de red aparecen en la captura?: El protocolo de red IP
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: El protocolo UDP y RTP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: No hay protocolos de aplicación en la captura.
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 229
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31be1e0e (834543118)
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): Lleva datos en su mayoría y sus respectivos indicadores.
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 1
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: A
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: Dura 12.81 segundos.
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.65 ms.
* ¿Cuál es el jitter medio del flujo?: 12.234 ms.
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes.
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 12.78 segundos aproximadamente.
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26643
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Con 30.04 ms de delta podemos decir que ha habido una diferencia notable respecto al anterior paquete. Gracias al skew de -9.59 ms, podemos saber que va con retraso.
* ¿Qué jitter se ha calculado para ese paquete? 12.336 ms
* ¿Qué timestamp tiene ese paquete? 18400
* ¿Por qué número hexadecimal empieza sus datos de audio? 7e
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: mar 24 oct 2023 22:20:19 CEST
* Número total de paquetes en la captura: 3671
* Duración total de la captura (en segundos): 14.49 segundos.
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450
* ¿Cuál es el SSRC del flujo?: 0xb690c78
* ¿En qué momento (en ms) de la traza comienza el flujo? 6912 ms.
* ¿Qué número de secuencia tiene el primer paquete del flujo? 22562
* ¿Cuál es el jitter medio del flujo?: 0 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes.
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
